package com.andrewjb.hexagons.Utils;

public class RenderManager {

    private float hexPadding;
    private float hexHeight;
    private float hexWidth;
    private float hexVerticalOffset;
    private float hexHorizontalOffset;

    public RenderManager(float canvasWidth, float canvasHeight, float canvasZoom) {
        hexPadding = canvasWidth / 4560;
        hexHeight = canvasZoom * (2 + hexPadding);
        hexWidth = (float) Math.sqrt(3)/2 * hexHeight;
        hexVerticalOffset = hexHeight * 3/4;
        hexHorizontalOffset = hexWidth / 2;
    }

    public float getHexPadding() {
        return hexPadding;
    }

    public float getHexHeight() {
        return hexHeight;
    }

    public float getHexWidth() {
        return hexWidth;
    }

    public float getHexVerticalOffset() {
        return hexVerticalOffset;
    }

    public float getHexHorizontalOffset() {
        return hexHorizontalOffset;
    }
}

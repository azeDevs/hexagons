package com.andrewjb.hexagons.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.andrewjb.hexagons.Objects.Grid;
import com.andrewjb.hexagons.Objects.Hexagon;
import com.andrewjb.hexagons.Utils.RenderManager;

public class GameView extends View {

    // Canvas
    private float canvasWidth;
    private float canvasHeight;
    private Grid grid;
    private RenderManager renderManager;

    public GameView(Context context) {
        super(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        gameInit();
    }

    private void gameInit() {
        canvasWidth = getWidth();
        canvasHeight = getHeight();

        grid = new Grid(canvasWidth / 2, canvasHeight / 2, canvasWidth / 8);
        renderManager = new RenderManager(canvasWidth, canvasHeight, grid.getCanvasZoom());

        makeAllTheHexs();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int i=0; i < grid.hexagons.size(); i++) {
            Hexagon currentHexagon = grid.getHexagon(i);
            currentHexagon.drawHex(canvas);
        }
    }

    //TODO: Generate this in Grid
    private void makeAllTheHexs() {
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX() - renderManager.getHexHorizontalOffset(), grid.getCanvasCenterY() - renderManager.getHexVerticalOffset(), "#bfdbd5"));
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX() + renderManager.getHexHorizontalOffset(), grid.getCanvasCenterY() - renderManager.getHexVerticalOffset(), "#bfdbd5"));
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX(), grid.getCanvasCenterY(), "#bfdbd5"));
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX() + renderManager.getHexWidth(), grid.getCanvasCenterY(), "#bfdbd5"));
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX() - renderManager.getHexWidth(), grid.getCanvasCenterY(), "#bfdbd5"));
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX() + renderManager.getHexHorizontalOffset(), grid.getCanvasCenterY() + renderManager.getHexVerticalOffset(), "#bfdbd5"));
        grid.addHexagon(new Hexagon(grid.getCanvasZoom(), grid.getCanvasCenterX() - renderManager.getHexHorizontalOffset(), grid.getCanvasCenterY() + renderManager.getHexVerticalOffset(), "#bfdbd5"));
    }
}

package com.andrewjb.hexagons.Objects;

import java.util.ArrayList;

public class Grid {

    //TODO: Have Grid generate all possible Hexagon centers upon creation based on proposed Grid size

    public ArrayList<Hexagon> hexagons;

    private float canvasCenterX;
    private float canvasCenterY;
    private float canvasZoom;

    public Grid(float canvasCenterX, float canvasCenterY, float canvasZoom) {
        this.canvasCenterX = canvasCenterX;
        this.canvasCenterY = canvasCenterY;
        this.canvasZoom = canvasZoom;
        hexagons = new ArrayList<Hexagon>();
    }

    public Hexagon getHexagon(int n) {
        return hexagons.get(n);
    }

    public void addHexagon(Hexagon hexagon) {
        hexagons.add(hexagon);
    }

    public float getCanvasCenterX() {
        return canvasCenterX;
    }

    public float getCanvasCenterY() {
        return canvasCenterY;
    }

    public float getCanvasZoom() {
        return canvasZoom;
    }
}

package com.andrewjb.hexagons.Objects;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public class Hexagon {

    public float size;
    public float centerX;
    public float centerY;
    public Paint colorSurface;
    public Paint colorShadow;

    public Hexagon(float size, float centerX, float centerY, String color) {
        this.size = size;
        this.centerX = centerX;
        this.centerY = centerY;
        this.colorSurface = makeSurfacePaint(color);
        this.colorShadow = makeShadowPaint(color);
    }

    //TODO: Fix this loop to handle passing in animations
    public void drawHex(Canvas canvas) {
        final int depth = (int) Math.floor((size * 0.1) * 1.5);
        final float[] x = new float[6];
        final float[] y = new float[6];

        for (int i=0; i<6; i++) {
            float angle_deg = 60 * i + 30;
            float angle_rad = (float) Math.PI / 180 * angle_deg;
            x[i] = centerX + size * (float) Math.cos(angle_rad);
            y[i] = (centerY + size * (float) Math.sin(angle_rad));
        }

        for (int offset = 0; offset <= depth; offset++) {
            Path wallpath = new Path();
            wallpath.reset();
            wallpath.moveTo(x[0], y[0] - offset);
            wallpath.lineTo(x[1], y[1] - offset);
            wallpath.lineTo(x[2], y[2] - offset);
            wallpath.lineTo(x[3], y[3] - offset);
            wallpath.lineTo(x[4], y[4] - offset);
            wallpath.lineTo(x[5], y[5] - offset);
            wallpath.lineTo(x[0], y[0] - offset);

            canvas.drawPath(wallpath, (offset < depth) ? this.colorShadow : this.colorSurface);
            canvas.drawText("Hexagon", centerX-size*0.5f, centerY-size*0.25f, this.colorShadow);
            canvas.drawText("X=" + Math.floor(centerX) + " Y=" + Math.floor(centerY), centerX-size*0.5f, centerY, this.colorShadow);
        }
    }

    //TODO: Merge these methods into a single method
    private Paint makeSurfacePaint(String color) {
        Paint hexPaint = new Paint();
        int hexColor = Color.parseColor(color);
        hexPaint.setARGB(255, Color.red(hexColor), Color.green(hexColor), Color.blue(hexColor));
        hexPaint.setAntiAlias(true);
        hexPaint.setStyle(Paint.Style.FILL);
        return hexPaint;
    }

    private Paint makeShadowPaint(String color) {
        Paint hexPaint = new Paint();
        int hexColor = Color.parseColor("#839595");
        hexPaint.setARGB(255, Color.red(hexColor), Color.green(hexColor), Color.blue(hexColor));
        hexPaint.setAntiAlias(true);
        hexPaint.setStyle(Paint.Style.FILL);
        hexPaint.setTextSize(20f);
        return hexPaint;
    }

}